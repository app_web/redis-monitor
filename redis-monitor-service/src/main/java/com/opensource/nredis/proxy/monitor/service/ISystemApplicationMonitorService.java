package com.opensource.nredis.proxy.monitor.service;

import com.opensource.nredis.proxy.monitor.model.SystemApplicationMonitor;

/**
* service interface
*
* @author liubing
* @date 2016/12/31 08:09
* @version v1.0.0
*/
public interface ISystemApplicationMonitorService extends IBaseService<SystemApplicationMonitor>,IPaginationService<SystemApplicationMonitor>  {
}

